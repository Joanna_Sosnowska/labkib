package pk.labs.LabB;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabB.display.Wyswietlacz";
    public static String controlPanelImplClassName = "pk.labs.LabB.controlpanel.PanelKontrolny";

    public static String mainComponentSpecClassName = "pk.labs.LabB.Contracts.Kuchenka";
    public static String mainComponentImplClassName = "pk.labs.LabB.main.Mikrofalowka";
    public static String mainComponentBeanName = "mikrofalowka";
    // endregion

    // region P2
    public static String mainComponentMethodName = "nowametoda";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "loggerImpl";
    // endregion
}
