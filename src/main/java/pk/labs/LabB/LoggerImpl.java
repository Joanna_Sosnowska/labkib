package pk.labs.LabB;

import java.util.Arrays;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import pk.labs.LabB.Contracts.Logger;
@Aspect
@Component
public class LoggerImpl implements Logger {
	private Logger logger;    
    
        public LoggerImpl() {
            this.logger = this;
        }
	public void logMethodEntrance(String methodName, Object[] methodArgs) {
		System.out.printf("Enter in %s with args %s\n", methodName, methodArgs != null ? Arrays.asList(methodArgs) : "[]");
	}
	
	public void logMethodExit(String methodName, Object methodResult) {
		System.out.printf("Exit from %s with result %s\n", methodName, methodResult);
	}

         


	
}
